# Yellow Dot Energy Market Connector

## Overview

Yellow Dot's solar installations are exposed to negative pricing events.  To avoid generating electricity in the face of negative prices, the installations need regular updates on the electricity market price.  In future, these solar installations will also make use of batteries for energy storage.  To optimise their charging/discharging behaviour, the installations will require a _history_ of market prices (and other information/calculated values besides).

Electricity market prices are obtained from AEMO.  The Yellow Dot AEMO market price connection will be implemented in Amazon Web Services.  It will be structured as per the following diagram:

```mermaid
graph TD
S1((Solar Plant A)) -->|Request Market Update| G(API Gateway)
S2((Solar Plant B)) -->|Request Market Update| G
G --> C(AEMO Client)
subgraph VPN
C -->|GET Market Pricing| A(AEMO REST API)
V[VPN Server]
end
W(Watchdog) -->|Reboot| V
D[Dashboard]
A -.-> D
C -.-> D
W -.-> D
```

It will be comprised of two servers:

- VPN router
- Operations Dashboard

In addition, the following "serverless" components will be present:

- AEMO client
- Watchdog
- API Gateway

The serverless components will use AWS technologies that allow implementation of custom logic, but don't involve running traditional servers (physical or virtual), thereby increasing reliability and reducing costs and operational overhead.

## Design Rationale

Two possible design approaches have been considered:

- Individual solar installations connect direct to AEMO
- Solar installations connect to centralised Yellow Dot infrastructure, which connects to AEMO

For long-lived connections, AEMO mandate the use of a VPN connection from a _single_ fixed IP address.  Their VPN requirements are strict, and fairly complex.  AEMO do (in theory) allow for temporary client VPN connections from variable address, but details on this facility have been difficult to obtain.  Dot Dash Code will continue to investigate this possibility, however.

Dot Dash Code are pursuing a solution with centralised infrastructure for the following reasons:

- Reliability - AWS services are extremely reliable.  Their managed services almost never experience outages, and the solution requires only one virtual "server" on the critical path of operation (the VPN router).  Any outages on this server will be managed by a watchdog process (details below).
- Visibility - The solution will provide a single dashboard for monitoring the installations' behaviour, with respect to market price updates (i.e. frequency of requests).  This will provide alerts in the face of unusual behaviour from the solar installations (e.g. no update requests within the last X minutes).  Such monitoring would be more difficult with a distributed architecture.
- Maintainability - The future addition of battery systems to the solar installations will require on-going development of the electricity market price feed.  In particular, they will require historical pricing information, and predicted future pricing.  This on-going development (and corresponding software deployments) will be easier to manage in a centralised system.

## Components

### Router

- AEMO requires market participants to connect via VPN
- They have strict controls around client network architecture
- They require clients to connect from a single, _fixed_ IP address which is configured in their system
- They provide guidelines on using Cisco CSR appliances (in AWS) as the VPN client
  - Problem is: the Cisco appliance is _hideously_ expensive to run, due to software licensing costs.  It will cost at least $4k per annum.
  - Dot Dash Code are currently exploring the use of Open VPN (no software licensing costs) as an alternative.

### Dashboard

- All components of the Energy Market Connector will report logs and metrics on their behaviour
- All logs and metrics will be reported/stored using Amazons CloudWatch facility.
- Metrics will include:
  - Market update request count/frequency from each installation
  - Current (and historical) per-state energy price
  - Operational error counts
  - Watchdog reboot counts
- Metrics will be visible to Yellow Dot staff via Grafana dashboards (https://grafana.com/)
- Yellow Dot staff will be able to configure their own dashboards
- Yellow Dot staff will be able to configure alerts based on metrics (e.g. send email when QLD market price drops below $1)

### AEMO client

The AEMO client connects to AEMO's API via the VPN.  Its execution is triggered by update requests from the solar installations, and it runs on Amazon's Lambda platform (for execution of code without dedicated servers - physical or virtual).

When executed, it will provide a response containing market data (for various states) in the following format:

```json
{
  "version": 1.0,
  "locations": [
    {
      "location": "QLD",
      "pricing": [
        {
          "price": 123.45,
          "timestamp": "2019-04-03T20:28:27Z"
        },
        {
          "price": 124.56,
          "timestamp": "2019-04-03T20:29:27Z"
        }
      ]
    },
    {
      "location": "NSW",
      "pricing": [
        {
          "price": 245.89,
          "timestamp": "2019-04-03T20:28:27Z"
        },
        {
          "price": 246.78,
          "timestamp": "2019-04-03T20:29:27Z"
        }
      ]
    }
  ]
}
```

These responses will be consumed by the controllers at the solar installations.  In the face of negative prices, the controllers can cease generating  electricity.

### API Gateway

The API Gateway will be implemented using Amazon's facility of the same name.  It will provide caching behaviour for market price updates (limiting the rate at which the AEMO client will run), and authentication functionality (each installation will have a unique key for accessing the API).

### Watchdog

The Watchdog executes using AWS Lambda, and is responsible for monitoring the VPN server and triggering a reboot in the face of any misbehaviour.

## Running Costs

Cisco license fees notwithstanding (see above), Dot Dash Code are targeting a running cost of AU$80 per month for this system.